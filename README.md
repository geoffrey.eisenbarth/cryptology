Synopsis
========
For educational purposes, I have written implementations of various crypto schemas in Python. It should go without saying that these are not to be depended on for security.

Dependencies
============
- Python 2.7

Code Example
===========
Show what the library does as concisely as possible, developers should be able to figure out **how** your project solves their problem by looking at the code example. Make sure the API you are showing off is obvious, and that your code is short and concise.

Motivation
==========
As a mathematician, I am very interested in understanding cryptosystems and wanted to explore recreating 
various ciphers in Python as well as implementing methods to crack a given cipher.

Installation
============
Using git: `git clone https://gitlab.com/geoffrey.eisenbarth/cryptology`

API Reference
=============
Depending on the size of the project, if it is small and simple enough the reference docs can be added to the README. For medium size to larger projects it is important to at least provide a link to where the API reference docs live.

Tests
=====
Describe and show how to run the tests with code examples.

Contributors
============
This was created as a personal project, but feel free to clone and/or add to it as you see fit! I am very 
interested in input from others and will happily accept pull requests that I feel encourage the spirit of 
the project!

License
=======
A short snippet describing the license (MIT, Apache, etc.)